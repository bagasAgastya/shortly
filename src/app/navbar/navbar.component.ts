import { Component, OnInit } from '@angular/core';
import { Collapse } from 'flowbite';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  targetEl: HTMLElement = document.getElementById('navbar-button');
  triggerEl: HTMLElement = document.getElementById('navbar-content');
  collapse = new Collapse(this.targetEl, this.triggerEl);

  constructor() { }

  ngOnInit(): void {}

  toggleNavbar() {
    return this.collapse?.toggle();
  }
}
