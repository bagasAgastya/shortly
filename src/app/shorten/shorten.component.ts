import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import Swal from 'sweetalert2';
import { ShortenService } from '../services/shorten.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-shorten',
  templateUrl: './shorten.component.html',
  styleUrls: ['./shorten.component.css'],
})
export class ShortenComponent implements OnInit, OnDestroy {
  private subsink = new SubSink();
  shortenURLCtrl = new FormControl(null);
  listShortenLink = [];
  selectedIndex;
  isLoading = false;

  constructor(private shortenService: ShortenService) {}

  ngOnInit(): void {
    this.listShortenLink = JSON.parse(localStorage.getItem('list'))
      ? JSON.parse(localStorage.getItem('list'))
      : [];
  }

  btnShortern() {
    this.isLoading = true;
    const isValid = this.shortenURLCtrl.value;
    if (!isValid) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'You need put an URL Link for Shorten the link!',
      }).then(() => {
        this.isLoading = false;
      });
    } else {
      this.subsink.sink = this.shortenService.addShortenLink(isValid).subscribe(
        (resp: any) => {
          if (resp && resp.result) {
            Swal.fire({
              icon: 'success',
              title: 'Bravo !',
              text: 'Your URL got shortened',
            }).then(() => {
              const link = {
                originalLink: resp?.result?.original_link,
                shortenLink: resp?.result?.full_short_link,
              };
              if (link) {
                this.listShortenLink = [{ ...link }, ...this.listShortenLink];
                localStorage.setItem(
                  'list',
                  JSON.stringify(this.listShortenLink)
                );
              }
              this.shortenURLCtrl.reset();
              this.isLoading = false;
            });
          }
        },
        (err) => {
          this.isLoading = false;
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: err?.error?.error,
          });
        }
      );
    }
  }

  async copyToClipboard(data, index) {
    if (navigator.clipboard) {
      this.selectedIndex = index;
      return await navigator.clipboard.writeText(data.shortenLink);
    }
  }

  resetSelectedIndex() {
    this.selectedIndex = null;
  }

  ngOnDestroy(): void {
    this.subsink.unsubscribe();
  }
}
