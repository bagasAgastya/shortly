import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShortenService {
  environment = 'https://api.shrtco.de/v2/shorten?url='

  constructor(private httpClient: HttpClient) { }

  addShortenLink(data) {
    return this.httpClient.get(`${this.environment + data}`);
  }
}
