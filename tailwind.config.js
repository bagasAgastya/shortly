/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}", "./node_modules/flowbite/**/*.js"],
  theme: {
    extend: {
      backgroundImage: {
        "desktop-shorten": "url(assets/images/bg-shorten-desktop.svg)",
      },
    },
    colors: {
      // Configure your color palette here
      transparent: "transparent",
      current: "currentColor",
      teal: {
        DEFAULT: "#2DD2BE",
        light: "#0D9488",
        dark: "#0F766E",
      },
      gray: {
        DEFAULT: "#1F2937",
        light: "#F3F4F6",
      },
      white: "#fff",
      purple: "#581C87",
    },
    screens: {
      xsm: "425px",
      sm: "768px",
      // => @media (min-width: 640px) { ... }

      md: "1024px",
      // => @media (min-width: 768px) { ... }

      lg: "1200px",
      // => @media (min-width: 1024px) { ... }

      xl: "1280px",
      // => @media (min-width: 1280px) { ... }

      "2xl": "1536px",
      // => @media (min-width: 1536px) { ... }
    },
  },
  plugins: [require("flowbite/plugin")],
};
